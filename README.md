# W08 AJAX Example

## How to use

Create a client that uses AJAX to update just part of a web page. 

* Create C:\44563\w08 folder.  In this root folder,
* Create a simple client application.
* Create and test the provided client.
* Change the client to call some other available service or REST call.
* Run your app.
* Take a screen shot of your ajax app running on your desktop.
* Take a screen shot of your client code. 

## To submit:

* Title your post "NN Lastname, Firstname - success" if successful.  Title your post "NN Lastname, Firstname" if you run into any problems or have any questions. 
* Embed your screenshots.
* Optional: Include a clickable link to your w08 repo in the cloud.
* Tell us how long this assignment took.
* Tell us if AJAX allows us to only update parts of our pages rather than requiring a whole page refresh (using a complete sentence.)
* List at two jQuery AJAX methods (see the recommended overview site for ideas).
* Tell us if you had any problems or if you have any questions. 




